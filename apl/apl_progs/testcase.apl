decl
	integer status;
	integer status1;
	integer status2;
	integer statusa;
	integer statusb;
	integer statusc;
	integer w1;
	integer w2;
	integer i;
	integer j;
enddecl
integer main()
{
	status=Create("a.dat");
	status=Create("b.dat");
	statusa=Open("a.dat");
	statusb=Open("b.dat");
    
	i=30;
	while(i<=60) do
		status=Write(statusa,i);
		i=i+5;
	endwhile;
	i=2;
	while(i<=40) do
		status=Write(statusb,i);
		i=i+2;
	endwhile;
    
    status=Seek(statusa,0);
	status=Seek(statusb,0);	
	status=Create("c.dat");
	statusc=Open("c.dat");
	status1=Read(statusa,w1);
	status2=Read(statusb,w2);
	i=0;
	j=0;
	while(i<7 && j<20) do
	    if(w1<w2) then
	        status=Write(statusc,w1);
	        i=i+1;
	        status1=Read(statusa,w1);
	    else
	        status=Write(statusc,w2);
	        j=j+1;
	        status2=Read(statusb,w2);
	    endif;
	endwhile;
	while(i<7) do
	    status=Write(statusc,w1);
	    status1=Read(statusa,w1);
	    i=i+1;
	endwhile;
	while(j<20) do
	    status=Write(statusc,w2);
	    status2=Read(statusb,w2);
	    j=j+1;
	endwhile;
	
	status=Close(statusa);
	status=Close(statusb);
	status=Close(statusc);		
	status=Delete("a.dat");
	status=Delete("b.dat");
	
	print("success");
	return 0;
}
