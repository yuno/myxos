decl
	integer status;
enddecl
integer main()
{
	status = Create("newfile.dat");
	print(status);
	status = Open("newfile.dat");
	print(status);
	status = Delete("newfile.dat");
	print(status);
	status = Close(0);
	print(status);
	status = Delete("blah.dat");
	print(status);
	return 0;
}
